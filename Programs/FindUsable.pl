###########################################
#
#Created to make sure backbone gets done
#This program checks for clusters that will
#Be usable down the line
############################################
use Data::Dumper;
if($#ARGV != 3){
	
		print "Error!: FindUsable.pl ListOfSeqs.txt ClusterToTest HowManyYouWant AdditionToOutput\n";
		die;
	
}
$Cluster = $ARGV[1];
$Number = $ARGV[2];
$Addition = $ARGV[3];
open(List, $ARGV[0])||die "Issue is List is missing?\n";
while($line = <List>){
	
		($name,$test) = ($line =~ m/(.*?)\t(.*?)\_.*/);
		$HASH{$name} = $test;
}
open(Cluster, $ARGV[1])||die "Issue is the clusters missing?";
while($line = <Cluster>){
	
	chomp $line;
	
	if($line =~ /^>/){
	
		$name = ($line =~ m/>(.*?)@.*/)[0];
		if(exists $HASH{$name}){
				
				$Fam = "$HASH{$name}";
				$NewHASH{$Fam} = $Fam;
		}
	}
}
$size = keys %NewHASH;
if($Number <= $size){
	
		print "All Good moving $Cluster to next round\n";
		system("cp $Cluster $Addition$Cluster");
	
}else{
	
		print "Not enough coverage\n";
	
} 
