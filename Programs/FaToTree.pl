system("mkdir MadeTrees/");
$count = 0;
$name = $ARGV[0];
#system("mafft --auto --amino --maxiterate 1000 --thread 2 $name > $name.aln");
#system("~/phyx/src/pxclsq -s $name.aln -o $name.aln-cln -p 0.1 -a");
open(File, "$name")||die "How?";
while($line = <File>){

	if ($line =~ />/){

		$count++;	
	}
}
if($count > 3){

	system("mafft --auto --amino --maxiterate 1000 --thread 36 $name > $name.aln");
	system("~/phyx/src/pxclsq -s $name.aln -o $name.aln-cln -p 0.1 -a");

	$name = "$name.aln-cln";
	if ($count < 3){

		system("raxml -T 18 -p 12345 -m PROTCATWAG -s $name -n $name\.tre");
		system("rm RAxML_result* RAxML_parsimony* RAxML_log* RAxML_info*");
		system("mv RAxML_bestTree\.$name\.tre $name\.tre");

	}else{

		system("fasttree -wag $name > $name\.tre");


	}
	system("mv $name $name\.tre $name\.aln $name\.aln-cln MadeTrees/");
}else{


	system("mv $name MadeTrees/");

}	
