#Code is really shotty but it gets the job done
#Take in a set of families and compare to another set of families
#The input files for each family should be in the format: FamilyName_*.fa
#Then the program is run by doing perl ClusterCombiner.pl FamilyName1_ FamilyName2_ OutfileName
use Data::Dumper;
$FileName = $ARGV[0];
$FileName2 = $ARGV[1];
$outname = $ARGV[2];
system("rm Blast_hits.txt");
system("for x in $FileName*;do perl NameChanger.pl \$x;done");
print "Made Database Files\n";
system("cat Temp\_* > Temporary_Database.fa");
system("rm Temp\_*");
system("makeblastdb -in Temporary_Database.fa -out Temporary_Database.fa -dbtype 'prot'");
print "Made Database\n";
system("for x in $FileName2*;do perl NameChanger.pl \$x;done");
print "Doing Blast\n";
system("for x in Temp\_*;do blastp -db Temporary_Database.fa -query \$x -evalue 1e3 -num_threads 2 -max_target_seqs 1 -out \$x.rawblastp -outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore';done");
print "Done Blast\n";
system("for x in *.rawblastp;do perl CheckBlasts.pl \$x;done");
print "Checked Blast\n";
system("rm Temp*");
print "Combined Clusters out file of matches called: Blast_hits.txt\n";
print "Making New Clusters\n";
open(File, "Blast_hits.txt")||die "Should have been created did you delete Blast_hits?";
$count = 0;
while($line = <File>){
	
	chomp $line;
	($cluster1,$cluster2) = split " ", $line,2;
	$FIRST_HASH{$cluster1} = $cluster1;
	$cluster2 =~ s/,$//;
	@array = split ",", $cluster2;
	$cat = "";
	foreach $x (0..$#array){
			$SECOND_HASH{$array[$x]} = $array[$x];
			$cat .= " $FileName$array[$x]\.fa";
	}
	#print "$FileName$cluster1\.fa $cat\n";
	system("cat $FileName2$cluster1\.fa $cat > $outname\_$count\.fa");
	$count++;
}
#Now Check if anything is completely left behind
system("ls $FileName* > First_Seqs.txt");
system("ls $FileName2* > Second_Seqs.txt");
#print Dumper(\%SECOND_HASH);
open(File, "First_Seqs.txt")||die "Don't delete Files!";
while($line = <File>){

		chomp $line;
		$name = ($line =~ m/.*?\_(.*?)\.fa/)[0];
		if(exists $SECOND_HASH{$name}){
			
			
		}else{
				print "No Hit: $line\n";
				system("cp $line $outname\_NoHit\_$line");
		}
		
	
}
open(File, "Second_Seqs.txt")||die "Don't delete Files!";
while($line = <File>){

		chomp $line;
		$name = ($line =~ m/.*?\_(.*?)\.fa/)[0];
		if(exists $FIRST_HASH{$name}){
			
			
		}else{
				print "No Hit: $line\n";
				system("cp $line $outname\_NoHit\_$line");
		}
		
	
}
system("rm Second_Seqs.txt First_Seqs.txt");

