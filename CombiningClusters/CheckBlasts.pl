open(out, ">>Blast_hits.txt")||die "Should not die, sorry not helpful";
$name = $ARGV[0];
$cut = ($name =~ m/.*?\_.*?\_(.*?)\.fa/)[0];
open(Blast, $ARGV[0])||die "No Blast File";
while($line = <Blast>){
	
		chomp $line;
		@array = split " ", $line;
		$cluster1 = ($array[0] =~ m/.*?\_(.*?)\_.*/)[0];
		$cluster2 = ($array[2] =~ m/.*?\_(.*?)\_.*/)[0];
		$HASH{$cluster2} = $cluster1;
}
if(%HASH){
	print out "$HASH{$cluster2} ";

	foreach $keys (keys %HASH){
	
		print out "$keys,";
	}
	print out "\n";
}
